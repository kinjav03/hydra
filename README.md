# Hydra

Hydra is a Discord client that runs natively, as opposed to via Electron, designed with the following goals in mind:
- Provide a high-quality, graphical Discord client that is completely free software
- Design said client to be lightweight, resource-efficient, and run natively
- Allow for easy customization of this client to suit the needs of the end-user

Development of Hydra is in its beginning stages, so be sure to stick around if you're interested in its progress!